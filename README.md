# program1

In terminal, use: __java App.java__ to find all the files with __.txt__ or __.java__ extensions in the current directory (recursively, breadth first), and output respective __.stats__ files in the current directory.
Use __java App.java pathname__ to search for and output __.stats__ files in the specified path parameter __pathname__.

Moreover, all the functionalities asked for in the assignment are present and work as expected.