// Robert Vakhtangov & Aleksandre Khorbaladze

package edu.sdsu.cs;

import java.util.Comparator;

public class Pair implements IPair {
    private final String token;
    private final Integer num;

    public Pair(String token, Integer num) {
        this.num = num;
        this.token = token;
    }

    @Override
    public String getToken() {
        return token;
    }

    @Override
    public Integer getNum() {
        return num;
    }

    @Override
    public String toString() {
        return '(' + this.token + "," + this.num + ")";
    }

    static class PairComparator implements Comparator<Pair>
    {
        public int compare(Pair p1, Pair p2) {
            return p1.getNum() - p2.getNum();
        }
    }
}
