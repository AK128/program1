// Robert Vakhtangov & Aleksandre Khorbaladze

package edu.sdsu.cs;

interface IFileProcessor {
    void processAllFiles();
}

