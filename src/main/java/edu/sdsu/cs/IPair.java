// Robert Vakhtangov & Aleksandre Khorbaladze

package edu.sdsu.cs;

public interface IPair {
    String getToken();
    Integer getNum();
    String toString();
}
