// Robert Vakhtangov & Aleksandre Khorbaladze

package edu.sdsu.cs;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FileProcessor implements IFileProcessor {
    private final List<File> listOfFiles;
    private final IFileManager fileManager;
    private final String pathName;
    private List<String> SDT;

    public FileProcessor(String pathName){
        this.fileManager = new FileManager(pathName);
        this.listOfFiles = this.fileManager.getListOfFiles();
        this.pathName = pathName;
    };

    @Override
    public void processAllFiles(){
        for (File f : listOfFiles){
            genTokens(f);

            int lll = longestLineLength(f);
            double avrgLineLength = Math.round(avrgLineLength(f)*100)*1.0/100;
            int numUniSDTokensCS = numUniSDTokensCS();
            int numUniSDTokensCI = numUniSDTokensCI();
            int numAllSDTokens = numAllSDTokens();
            String mostFreqToken = mostFreqToken();
            int numMostFreqTokenCI = numMostFreqTokenCI();

            ArrayList<Pair> sortedPairs = getSortedPairs();
            int limit = Math.min(sortedPairs.size(), 10);
            List<Pair> mostFreqTokens = sortedPairs.subList(sortedPairs.size()-limit, sortedPairs.size());
            List<Pair> leastFreqTokens = sortedPairs.subList(0, limit);

            List<String> toWrite = new ArrayList<>();
            toWrite.add(fmt("Longest line length",String.valueOf(lll)));
            toWrite.add(fmt("Average line length",String.valueOf(avrgLineLength)));
            toWrite.add(fmt("Number of unique space-delineated tokens (case-sensitive)",
                    String.valueOf(numUniSDTokensCS)));
            toWrite.add(fmt("Number of unique space-delineated tokens (case-insensitive)",
                    String.valueOf(numUniSDTokensCI)));
            toWrite.add(fmt("Number of all space-delineated tokens in file",
                    String.valueOf(numAllSDTokens)));
            toWrite.add(fmt("Most frequently occurring token",mostFreqToken));
            toWrite.add(fmt("Count of most frequently occurring token (case-insensitive)",
                    String.valueOf(numMostFreqTokenCI)));
            toWrite.add(fmt("10 most frequent tokens with their counts (case-insensitive)",
                    String.valueOf(mostFreqTokens)));
            toWrite.add(fmt("10 least frequent tokens with their counts (case-insensitive)",
                    String.valueOf(leastFreqTokens)));

            String path = pathName+f.getName()+".stats";
            fileManager.writeToFile(path,toWrite);
        }
    }

    private String fmt(String key, String val){
        return String.format("%-64s : %-10s\n", key, val);
    }

    private void genTokens(File f){
        List<String> lines = fileManager.readFile(f);
        List<String> SDT = new ArrayList<>();
        for (String line : lines) {
            String[] lineArr = line.split(" ");
            Collections.addAll(SDT, lineArr);
        }
        SDT = SDT.stream().filter(c -> !c.equals("")).collect(Collectors.toList());
        this.SDT = SDT;
    }

    private int longestLineLength(File f) {
        int lll = 0;
        List<String> lines = fileManager.readFile(f);
        for (String l : lines) {
            lll = Math.max(l.length(), lll);
        }
        return lll;
    }

    private double avrgLineLength(File f) {
        double avrg = 0;
        int lengthSum = 0;
        List<String> lines = fileManager.readFile(f);
        int lengthOfListOfFiles = lines.size();
        for (String l : lines) {
            lengthSum += l.length();
        }
        avrg = lengthSum*1.0 / lengthOfListOfFiles;
        return avrg;
    }

    private int numUniSDTokensCS() {
        List<String> tempSDT = new ArrayList<>(SDT);
        tempSDT = tempSDT.stream().distinct().collect(Collectors.toList());
        return tempSDT.size();
    }

    private int numUniSDTokensCI() {
        List<String> tempSDT = new ArrayList<>(SDT);
        tempSDT = tempSDT.stream().map(String::toLowerCase).distinct().collect(Collectors.toList());
        return tempSDT.size();
    }

    private int numAllSDTokens() {
        return SDT.size();
    }

    private String mostFreqToken() {
        int count = 0;
        String freq = "";
        for (String token : SDT) {
            int tempCount = 0;
            for (String innerToken : SDT) {
                if (token.toLowerCase().equals(innerToken.toLowerCase())) tempCount++;
            }
            if (tempCount>count){
                count=tempCount;
                freq=token;
            }
        }
        return freq;
    }

    private int numMostFreqTokenCI() {
        int count = 0;
        for (String token : SDT) {
            int tempCount = 0;
            for (String innerToken : SDT) {
                if (token.toLowerCase().equals(innerToken.toLowerCase())) tempCount++;
            }
            count = Math.max(count,tempCount);
        }
        return count;
    }

    private ArrayList<Pair> getSortedPairs(){
        ArrayList<Pair> freqTokens = new ArrayList<Pair>();
        ArrayList<String> tokens = new ArrayList<String>();
        ArrayList<Integer> counts = new ArrayList<Integer>();
        int currentTokenCount = 0;

        SDT = SDT.stream().map(String::toLowerCase).collect(Collectors.toList());
        for(int i = 0; i < SDT.size(); i++){
            if(tokens.contains(SDT.get(i))){ continue; }
            currentTokenCount = 0;
            tokens.add(SDT.get(i));
            for(int j = i; j < SDT.size(); j++) {
                if(SDT.get(i).toLowerCase().equals(SDT.get(j).toLowerCase())){
                    currentTokenCount++;
                }
            }
            counts.add(currentTokenCount);
        }
        for(int i = 0; i < tokens.size(); i++) {
            freqTokens.add(new Pair(tokens.get(i), counts.get(i)));
        }
        Collections.sort(freqTokens, new Pair.PairComparator());
        return freqTokens;
    }
}