// Robert Vakhtangov & Aleksandre Khorbaladze

package edu.sdsu.cs;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class FileManager implements IFileManager {
    private String pathname;

    public FileManager(String pathname){
        this.pathname = pathname;
    }

    @Override
    public List<File> getListOfFiles() {
        List<File> listOfFiles = new ArrayList<File>();
        Queue<File> dirs = new LinkedList<File>();
        dirs.add(new File(pathname));
        while (!dirs.isEmpty()) {
            for (File f : Objects.requireNonNull(dirs.poll().listFiles())) {
                if (f.isDirectory()) {
                    dirs.add(f);
                } else if (f.isFile()) {
                    if (f.getName().endsWith(".txt") || f.getName().endsWith(".java"))
                        listOfFiles.add(f);
                }
            }
        }
        return listOfFiles;
    }

    @Override
    public List<String> readFile(File file) {
        List<String> lines = new ArrayList<>();
        try {
            Scanner myReader = new Scanner(file);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                lines.add(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return lines;
    }

    @Override
    public void writeToFile(String pathName, List<String> list) {
        try {
            Files.write(Paths.get(pathName),list,Charset.defaultCharset());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getPathname() {
        return pathname;
    }

    public void setPathname(String pathname) {
        this.pathname = pathname;
    }
}
