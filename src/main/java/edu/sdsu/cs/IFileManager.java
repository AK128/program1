// Robert Vakhtangov & Aleksandre Khorbaladze

package edu.sdsu.cs;

import java.io.File;
import java.nio.file.Path;
import java.util.List;

public interface IFileManager {
    public List<File> getListOfFiles();
    public List<String> readFile(File file);
    public void writeToFile(String pathName, List<String> list);
}
