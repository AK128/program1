// Robert Vakhtangov & Aleksandre Khorbaladze

package edu.sdsu.cs;

public class App
{
    public static void main( String[] args )
    {
        String pathname = System.getProperty("user.dir");
        if (args.length>0){
            pathname = args[0];
        }
        if (!pathname.endsWith("/")) pathname += "/";

        IFileProcessor fileProcessor = new FileProcessor(pathname);
        fileProcessor.processAllFiles();
    }
}